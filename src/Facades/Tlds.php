<?php namespace Hampel\Tlds\Facades;
/**
 * 
 */

use Illuminate\Support\Facades\Facade;

class Tlds extends Facade {

    protected static function getFacadeAccessor() { return 'tlds'; }

}
